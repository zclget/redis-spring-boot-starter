package org.zcl.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: ZCL
 * @since: 2021/10/13 17:00
 */
@ConfigurationProperties(prefix = "gp.redisson")
@Data
public class RedissonProperties {
    private String host;
    private String password;
    private int port;
    private int timeout;
    private boolean ssl;

}
