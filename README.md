# redis-spring-boot-starter

#### 介绍
自定义starter

#### 软件架构
软件架构说明


#### 安装教程


1. 创建springboot项目，去掉主启动类，去掉test目录，去掉spring-boot-maven-plugin插件（此插件的作用是用于springboot依靠java -jar启动时可以找到主启动类）
2. 添加自动装配pom依赖
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-configuration-processor</artifactId>
    <optional>true</optional>
</dependency>
```
3. 在resources下添加META-INF文件夹，添加spring.factories文件
4. 添加需要自动装配的bean配置类[RedissonAutoConfiguration](https://gitee.com/zclget/redis-spring-boot-starter/blob/master/src/main/java/org/zcl/config/RedissonAutoConfiguration.java)
5. 在spring.factories中添加配置org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
   org.zcl.config.RedissonAutoConfiguration
6. mvn clean install

#### 使用说明

在新建的项目中引入依赖
```xml
<dependency>
   <groupId>org.zcl</groupId>
   <artifactId>redis-spring-boot-starter</artifactId>
   <version>0.0.1-SNAPSHOT</version>
</dependency>
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
